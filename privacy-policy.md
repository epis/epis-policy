# Epis Privacy Policy

**Effective Date: July 3, 2018**

This policy explains what information we collect when you use Epis' sites, services, applications, products, and content ("Services"). It also has information about how we store, use, transfer, and delete that information. We take your privacy seriously and we hope to earn your trust in maintaining this information.

#### Information We Collect & How We Use It

Epis doesn't make money from ads. So we don't collect data in order to advertise to you. The tracking we do at Epis is to make our product work as well as possible.

In order to give you the best possible experience using Epis, we collect information from your interactions with our network. Some of this information, you actively tell us (such as your email address, which we use to track your account or communicate with you). Other information, we collect based on actions you take while using Epis, such as what pages you access and your interactions with our product features (like loves, comments, shares, etc). This information includes records of those interactions, your Internet Protocol address, information about your device (such as device or browser type), and referral information.

We use this information to:

*   provide, test, improve, promote and personalize Epis Services
*   fight spam and other forms of abuse
*   generate aggregate, non-identifying information about how people use Epis Services

When you create your Epis account, and authenticate with a third-party service (like Twitter, Facebook or Google) we may collect, store, and periodically update information associated with that third-party account, such as your lists of friends or followers. We will never publish through your third-party account without your permission.

#### Information Disclosure

Epis won't transfer information about you to third parties for the purpose of providing or facilitating third-party advertising to you. We won't sell information about you.

We may share your account information with third parties in some circumstances, including: (1) with your consent; (2) to a service provider or partner who meets GDPR data protection standards; (3) when we have a good faith belief it is required by law, such as pursuant to a subpoena or other legal process; (4) when we have a good faith belief that doing so will help prevent imminent harm to someone.

If we are going to share your information in response to legal process, we'll give you notice so you can challenge it (for example by seeking court intervention), unless we're prohibited by law or believe doing so may endanger others. We will object to requests for information about users of our services that we believe are improper.

#### Data Storage

Epis uses third-party vendors and hosting partners, such as Google, for hardware, software, networking, storage, and related technology we need to run Epis. We maintain two types of logs server logs and event logs. By using Epis Services, you authorize Epis to transfer, store, and use your information in the United States and any other country where we operate.

#### Tracking & Cookies

We use browser cookies and similar technologies to recognize you when you return to our Services. We use them in various ways, for example to log you in, remember your preferences (such as default language), evaluate email effectiveness and personalize content and other information.

Epis doesn't track you across the Internet. We track only your interactions within the Epis network (which encompasses Epis.io and custom domains hosted by Epis).
