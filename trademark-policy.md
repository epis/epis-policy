# Epis' Trademark Policy

People write about all kinds of things on Epis. Epis doesn't allow communication using someone else's trademark in a way likely to confuse people.

#### **What's a trademark?**

A trademark can be lots of things, but usually it's a word, phrase or design (like a company name, a product name, a slogan or a logo). It shows you that this good or service comes from one company as opposed to some other company.

Trademarks exist to prevent consumer confusion over the source of a particular good or service. They also stop one company from free-riding on another company's investment in branding. By contrast, they do not exist to help a company stop someone from discussing it (or its products) or to end secondary markets.

#### **Someone's using my trademark on Epis. What do I do?**

If you think someone's using your trademark in a way that's likely to confuse readers, let us know. We'll look into it. Then we'll take appropriate action. This might include temporarily or permanently removing a post or account.

If you think someone is infringing your trademark, send the following information to **trademark@epis.io**:

1.  Your contact information:

_Name  
Mailing address  
Email address  
Phone number_

**Please note**: we forward a trademark notice to the user who posted the disputed content. This might include your contact information, the name of your organization, and the contents of your notice. To protect your privacy, you may want to provide business rather than personal contact information.

2\. The trademarked word or symbol

3\. The trademark registration number and country where you claim trademark rights.

4\. The category of goods and/or services in which you assert rights.

5\. A link to the content on Epis that you think infringes.

6\. A description of how particular content may create confusion. The more concrete, the better.

7\. The action you request (e.g., removal of a particular post).

8\. If you're not the rights holder, describe your relationship to the rights holder.

9\. A statement that:

*   You have a good faith belief that use of the trademark is not authorized by the trademark owner
*   You have determined in good faith after a reasoned analysis that the use of the trademark is not fair use
*   The information in your notice is accurate

#### **If someone's using my trademark, in what scenarios would you not take it down?**

*   If they're using it in a way not likely to confuse anyone about the source of a good/service.
*   If they are making fair use of it, such as  [nominative fair use](https://en.wikipedia.org/wiki/Nominative_use) . This is when someone uses a trademark to refer to a company or brand, not to indicate it is the source of a good or service. For example, if someone writes a story about Company X and in that story uses the phrase "Company X" to refer to "Company X," that's probably fair use. Even if "Company X" is your trademark. People are allowed to use your company or product name to write things, even negative things, about your company/product/brand. As long as they're not confusing anyone about whether you authorized or endorse the post.

If you have questions or comments,  [let us know](mailto:trademark@epis.io) .
