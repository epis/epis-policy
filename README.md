# Epis' Policy
This is where we keep past and current versions of Epis' Policies and Guidelines.  

We aim to be transparent in our updates to our policies. Therefore we'll attempt to keep the most up to date changes in this repository for view and download.

### Current Policies
1. [Privacy Policy](./privacy-policy.md)
2. [Terms of Service](./terms-of-service.md)
3. [Rules](./rules.md)
4. [No Exploitation Policy](./no-exploitation-policy.md)
5. [Trademark Policy](./trademark-policy.md)
6. [Copyright and DMCA Policy](./copyright-and-dmca-policy.md)
