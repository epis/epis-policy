# Epis Terms of Service

**Effective: July 3, 2018**

These Terms of Service ("Terms") are a contract between you and Epis L.L.C. (hereafter referred to as "Epis") They govern your use of Epis' sites, services, apps, products, and content ("Services").

By using Epis, you agree to these Terms. If you don't agree to any of the Terms, you can not use Epis.

We can change these Terms at any time. We keep a  [historical](https://gitlab.com/epis/epis-policy) record of all changes to our Terms publicly accessible on GitLab. If a change is material, we'll let you know before they take effect. By using Epis on or after that effective date, you agree to the new Terms. If you don't agree to them, you should delete your account before they take effect, otherwise your continued use of the site and content will be subject to the new Terms.

#### Content rights & responsibilities

**<u>You own the rights to the content you create and post on Epis.</u>**

By posting content to Epis, you give us a nonexclusive license to publish it on Epis Services, including anything reasonably related to publishing it (like storing, displaying, reformatting, and distributing it). In consideration for Epis granting you access to and use of the Services, you agree that Epis may enable advertising on the Services, including in connection with the display of your content or other information. We may also use your content to promote Epis, including its products and content. We will never sell your content to third parties without your explicit permission.

You're responsible for the content you post. This means you assume all risks related to it, including someone else's reliance on its accuracy, or claims relating to intellectual property, copyrights, or other legal rights.

You're welcome to post content on Epis that you've published elsewhere, as long as you have the rights you need to do so. By posting content to Epis, you represent that doing so doesn't conflict with any other agreement you've made.

By posting content you didn't create to Epis, you are representing that you have the right to do so. For example, you are posting a work that's in the public domain, used under license (including a free license, such as  [Creative Commons](https://creativecommons.org/licenses/) ), or a fair use.

We can remove any content you post for any reason.

You can delete any of your posts, or your account, anytime. Processing the deletion may take some time, but we'll do it as quickly as possible.

#### Our content and services

We reserve all rights in Epis' look and feel. You may not copy or adapt any portion of our code or visual design elements (including logos) without express written permission from Epis unless otherwise permitted by law.

You may not do, or try to do, the following: (1) access or tamper with non-public areas of the Services, our computer systems, or the systems of our technical providers; (2) access or search the Services by any means other than the currently available, published interfaces (e.g., APIs) that we provide; (3) forge any TCP/IP packet header or any part of the header information in any email or posting, or in any way use the Services to send altered, deceptive, or false source-identifying information; or (4) interfere with, or disrupt, the access of any user, host, or network, including sending a virus, overloading, flooding, spamming, mail-bombing the Services, or by scripting the creation of content or accounts in such a manner as to interfere with or create an undue burden on the Services.

Crawling the Services is allowed if done in accordance with the provisions of our robots.txt file, but scraping or using any technical means to copy the content is prohibited.

We may change, terminate, or restrict access to any aspect of the service, at any time, without notice.

#### No children

Epis is only for people 13 years old and over. By using Epis, you affirm that you are over 13\. If we learn someone under 13 is using Epis, we'll terminate their account.

#### Incorporated rules and policies

By using the Services, you agree to let Epis collect and use information as detailed in our  [Privacy Policy](./privacy-policy.md) . If you're outside the United States, you consent to letting Epis transfer, store, and process your information (including your personal information and content) in and out of the United States.

To enable a functioning community, we have  [Rules](./rules.md) .  [DMCA Policy](/copyright-and-dmca-policy) , we'll remove material after receiving a valid takedown notice. Under our  aTrademark Policy](./trademark-policy.md) , we'll investigate any use of another's trademark and respond appropriately.

By using Epis, you agree to follow these Rules and Policies. If you don't, we may remove content, or suspend or delete your account.

#### Miscellaneous

_Disclaimer of warranty._ Epis provides the Services to you as is. You use them at your own risk and discretion. That means they don't come with any express or implied warranty.

_Limitation of Liability_. Epis will not be liable to you for any damages that arise from your using the Services. This includes if the Services are hacked or unavailable. This includes all types of damages (indirect, incidental, consequential, special or exemplary). And it includes all kinds of legal claims, such as breach of contract, breach of warranty, tort, or any other loss.

_No waiver._ If Epis doesn't exercise a particular right under these Terms, that doesn't waive it.

_Severability_. If any provision of these terms is found invalid by a court of competent jurisdiction, you agree that the court should try to give effect to the parties' intentions as reflected in the provision and that other provisions of the Terms will remain in full effect.

_Choice of law and jurisdiction._ These Terms are governed by California law, without reference to its conflict of laws provisions. You agree that any suit arising from the Services must take place in a court located in Los Angeles, California.

_Entire agreement._ These Terms (including any document incorporated by reference into them) are the whole agreement between Epis and you concerning the Services.
